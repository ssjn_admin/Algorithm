﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TestMain
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        private void tsbMultlineFit_Click(object sender, EventArgs e)
        {
            FormMultiLineFit formChild = new FormMultiLineFit();
            formChild.TopLevel = false;
            formChild.FormBorderStyle = FormBorderStyle.None;
            this.toolStripContainer1.ContentPanel.Controls.Add(formChild);
            formChild.Show();
            formChild.Dock = DockStyle.Fill;
        }
    }
}
