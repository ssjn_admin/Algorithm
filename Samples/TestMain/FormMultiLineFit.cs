﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using AlgorithmMain;

namespace TestMain
{
    public partial class FormMultiLineFit : Form
    {
        public FormMultiLineFit()
        {
            InitializeComponent();
        }

        private void FormMultiLineFit_Load(object sender, EventArgs e)
        {
            DataTable dataTable = getTestData();
            this.dataGridView1.DataSource = dataTable.DefaultView;
            
        }
        private DataTable getTestData()
        {
            DataTable dataTable = new DataTable("testdata");
            dataTable.Columns.Add(new DataColumn("X", typeof(System.Double)));
            dataTable.Columns.Add(new DataColumn("Y", typeof(System.Double)));
            DataRow drNew = dataTable.NewRow();
            drNew[0] = 1; drNew[1] = 31.3;
            dataTable.Rows.Add(drNew);

            drNew = dataTable.NewRow();
            drNew[0] = 2; drNew[1] = 29.14;
            dataTable.Rows.Add(drNew);

            drNew = dataTable.NewRow();
            drNew[0] = 3; drNew[1] = 31.3;
            dataTable.Rows.Add(drNew);

            drNew = dataTable.NewRow();
            drNew[0] = 4; drNew[1] = 26.83;
            dataTable.Rows.Add(drNew);

            drNew = dataTable.NewRow();
            drNew[0] = 5; drNew[1] = 23.38;
            dataTable.Rows.Add(drNew);

            drNew = dataTable.NewRow();
            drNew[0] = 6; drNew[1] = 33.9;
            dataTable.Rows.Add(drNew);

            drNew = dataTable.NewRow();
            drNew[0] = 7; drNew[1] = 29.21;
            dataTable.Rows.Add(drNew);

            drNew = dataTable.NewRow();
            drNew[0] = 8; drNew[1] = 27.83;
            dataTable.Rows.Add(drNew);

            drNew = dataTable.NewRow();
            drNew[0] = 9; drNew[1] = 23.79;
            dataTable.Rows.Add(drNew);

            drNew = dataTable.NewRow();
            drNew[0] = 10; drNew[1] = 29.82;
            dataTable.Rows.Add(drNew);

            drNew = dataTable.NewRow();
            drNew[0] = 11; drNew[1] = 29.74;
            dataTable.Rows.Add(drNew);

            drNew = dataTable.NewRow();
            drNew[0] = 12; drNew[1] = 27.36;
            dataTable.Rows.Add(drNew);

            drNew = dataTable.NewRow();
            drNew[0] = 13; drNew[1] = 32.16;
            dataTable.Rows.Add(drNew);

            drNew = dataTable.NewRow();
            drNew[0] = 14; drNew[1] = 33.51;
            dataTable.Rows.Add(drNew);

            drNew = dataTable.NewRow();
            drNew[0] = 15; drNew[1] = 32.78;
            dataTable.Rows.Add(drNew);

            drNew = dataTable.NewRow();
            drNew[0] = 16; drNew[1] = 33.95;
            dataTable.Rows.Add(drNew);

            drNew = dataTable.NewRow();
            drNew[0] = 17; drNew[1] = 29.72;
            dataTable.Rows.Add(drNew);

            drNew = dataTable.NewRow();
            drNew[0] = 18; drNew[1] = 26.82;
            dataTable.Rows.Add(drNew);

            drNew = dataTable.NewRow();
            drNew[0] = 19; drNew[1] = 25.68;
            dataTable.Rows.Add(drNew);

            drNew = dataTable.NewRow();
            drNew[0] = 20; drNew[1] = 27.93;
            dataTable.Rows.Add(drNew);

            drNew = dataTable.NewRow();
            drNew[0] = 21; drNew[1] = 29.27;
            dataTable.Rows.Add(drNew);

            drNew = dataTable.NewRow();
            drNew[0] = 22; drNew[1] = 30.1;
            dataTable.Rows.Add(drNew);

            drNew = dataTable.NewRow();
            drNew[0] = 23; drNew[1] = 27.95;
            dataTable.Rows.Add(drNew);

            drNew = dataTable.NewRow();
            drNew[0] = 24; drNew[1] = 28.44;
            dataTable.Rows.Add(drNew);

            drNew = dataTable.NewRow();
            drNew[0] = 25; drNew[1] = 26.63;
            dataTable.Rows.Add(drNew);

            drNew = dataTable.NewRow();
            drNew[0] = 26; drNew[1] = 29.84;
            dataTable.Rows.Add(drNew);

            drNew = dataTable.NewRow();
            drNew[0] = 27; drNew[1] = 28.4;
            dataTable.Rows.Add(drNew);

            drNew = dataTable.NewRow();
            drNew[0] = 28; drNew[1] = 21.37;
            dataTable.Rows.Add(drNew);

            return dataTable;
        }

        private void btnPlay_Click(object sender, EventArgs e)
        {
            DataTable dataTable = ((DataView)this.dataGridView1.DataSource).Table;
            int nRowCount = dataTable.Rows.Count;
            double[] daX = new double[nRowCount];
            double[] daY = new double[nRowCount];
            for (int i = 0; i < nRowCount; i++)
            {
                daX[i] = Convert.ToDouble(dataTable.Rows[i][0]);
                daY[i] = Convert.ToDouble(dataTable.Rows[i][1]);
            }
            int nTrendTimes = 5; // 函数次数
            double[] daFactor = MultiLineFit.MultiLine(daX, daY, nRowCount, nTrendTimes);

            //String strFactor = "";
            //for(int i = 0; i < daFactor.Length; i++)
            //{
            //    strFactor += (daFactor[i] + "\t");
            //}
            //this.txtFactor.Text = strFactor;

            this.chart1.Series.Clear();

            Series series1 = new Series();
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            //series1.Legend = "原始数据";
            series1.Name = "原始数据";
            series1.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;

            for (int i = 0; i < nRowCount; i++)
            {
                // Add X and Y values for a point. 
                series1.Points.AddXY(daX[i], daY[i]);
            }
            this.chart1.Series.Add(series1);

            String strFomula = "Y=" + String.Format("{0:0.####}", daFactor[0]) + "+";
            for (int j = 1; j < daFactor.Length - 1; j++)
            {
                strFomula += String.Format("{0:0.####}", daFactor[j]) + "*X^" + j + "+";
            }
            strFomula += (String.Format("{0:0.####}", daFactor[daFactor.Length - 1])
                + "*X^" + (daFactor.Length - 1));
            this.txtFactor.Text = strFomula;

            double[] daYFit = new double[nRowCount];
            for (int i = 0; i < nRowCount; i++)
            {
                //daYFit[i] = daFactor[0] + daFactor[1] * daX[i] + daFactor[2] * daX[i] * daX[i] + daFactor[3] * daX[i] * daX[i] * daX[i];
                daYFit[i] = daFactor[0];// - dAu;
                for (int j = 1; j < daFactor.Length; j++)
                {
                    daYFit[i] += daFactor[j] * Math.Pow(daX[i], j);
                }
            }
            Series series2 = new Series();
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            //series1.Legend = "原始数据";
            series2.Name = "拟合数据";
            series2.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            for (int j = 0; j < nRowCount; j++)
            {
                series2.Points.AddXY(daX[j], daYFit[j]);
            }
            this.chart1.Series.Add(series2);
        }
    }
}
